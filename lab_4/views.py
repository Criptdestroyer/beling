from django.http import HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime, date

response = {}

# Create your views here.
def index(request):
    response['name'] = 'Ahmad Emir Alfatah'
    return render(request, "index_lab1.html", response)