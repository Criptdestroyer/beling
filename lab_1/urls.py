from django.urls import re_path
from .views import index, message_post, message_table
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'message_post', message_post, name='message_post'),
    re_path(r'message_table', message_table, name="message_table")
]


