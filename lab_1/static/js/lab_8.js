
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384378035486719',
      cookie     : true,
      xfbml      : true,
      version    : 'v4.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   const render = loginFlag => {
        if (loginFlag) {
            Facebook.getUserData(user => {
                $('#lab8').html(
                    '<div class="profile">' +
                    '<img class="cover" src="' + user.cover.source + '" alt="cover" />'
                    +
                    '<img class="picture" src="' + user.picture.data.url + '"alt="profpic" />' +
                    '<div class="data">' +
                    '<h1>' + user.name + '</h1>' +
                    '<h2>' + user.about + '</h2>' +
                    '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
                    '</div>' +
                    '</div>' +
                    '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
                    '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
                    '<button class="logout" onclick="facebookLogout()">Logout</button>'
                );

                getUserFeed(feed => {
                    feed.data.map(Value => {
                        if (Value.message && Value.story) {
                            $('#lab8').append(
                                '<div class="feed">' +
                                '<h1>' + Value.message + '</h1>' +
                                '<h2>' + Value.story + '</h2>' +
                                '</div>'
                            );
                        } else if (Value.message) {
                            $('#lab8').append(
                                '<div class="feed">' +
                                '<h1>' + Value.message + '</h1>' +
                                '</div>'
                            );
                        } else if (Value.story) {
                            $('#lab8').append(
                                '<div class="feed">' +
                                '<h2>' + Value.story + '</h2>' +
                                '</div>'
                            );
                        }
                    });
                });
            });
        } else {
            $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
        }
    };

    const facebookLogin = () => {
        // TODO: Implement Method Ini
        // Pastikan method memiliki callback yang akan memanggil fungsi rendertampilan sudah login
        // ketika login sukses, serta juga fungsi ini memiliki segala permissionyang dibutuhkan
        // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    };

    const facebookLogout = () => {
        // TODO: Implement Method Ini
        // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
        // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    };

    // TODO: Lengkapi Method Ini
    // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
    // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
    // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
    // meneruskan response yang didapat ke fungsi callback tersebut
    // Apakah yang dimaksud dengan fungsi callback?
    const getUserData = (fun) => {
        // ...
        FB.api('/me?fields=....', 'GET', function (response){
            fun(response);
        });
        // ...
    };

    const getUserFeed = (fun) => {
        // TODO: Implement Method Ini
        // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
        // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
        // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
        // tersebut
    };

    const postFeed = () => {
        // Todo: Implement method ini,
        // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
        // Melalui API Facebook dengan message yang diterima dari parameter.
    };

    const postStatus = () => {
        const message = $('#postInput').val();
        postFeed(message);
    };

function facebookLogin(){
    FB.login(function(response){
        console.log(response);
    }, {scope:'public_profile,user_posts,publish_actions'})
}

function getUserData(){
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.api('/me?fields=id,name', 'GET', function(response){
                console.log(response);
            });
        }
    });
}

function postFeed(){
    var message = "Hello World!";
    FB.api('/me/feed', 'POST', {message:message});
}

function facebookLogout(){
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout();
        }
    });
}

